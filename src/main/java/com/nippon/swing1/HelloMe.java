/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nippon.swing1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class MyActionListener implements ActionListener {//event
    
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener : Action");
    }
    
}

/**
 *
 * @author Nippon
 */
public class HelloMe implements ActionListener {
    
    public static void main(String[] args) {
        JFrame framemain = new JFrame("Hello Me");
        framemain.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//JFrame
        framemain.setSize(500, 300); //window
        
        JLabel lblYourName = new JLabel("Your Name : ");
        lblYourName.setSize(80, 20); //component
        lblYourName.setLocation(5, 5);//component
        lblYourName.setBackground(Color.WHITE);//Jcomponent
        lblYourName.setOpaque(true);//Jcomponent
        
        JTextField txtYourName = new JTextField();
        txtYourName.setSize(200, 20);//component
        txtYourName.setLocation(90, 5);//component
        
        JButton btnHello = new JButton("Hello"); //JButton
        btnHello.setSize(80, 20);//component
        btnHello.setLocation(90, 40);//component
        
        MyActionListener action = new MyActionListener(); //extends object implements ActionListener
        btnHello.addActionListener(action); //abstract Button
        btnHello.addActionListener(new HelloMe());//abstract Button
        
        ActionListener actionListener = new ActionListener() { //event
            @Override
            public void actionPerformed(ActionEvent e) {  //Anonymous class
                System.out.println("Anonymous Class : Action");
            }
            
        };
        btnHello.addActionListener(actionListener); //abstract Button
        
        JLabel lblHello = new JLabel("Hello.....", JLabel.CENTER);
        lblHello.setSize(200, 20);//component
        lblHello.setLocation(90, 70);//component
        lblHello.setBackground(Color.WHITE);//Jcomponent
        lblHello.setOpaque(true);//Jcomponent
        
        framemain.setLayout(null);
        
        framemain.add(lblYourName);//Container
        framemain.add(txtYourName);
        framemain.add(btnHello);
        framemain.add(lblHello);
        
        btnHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { //connet event
                String name = txtYourName.getText(); //JTextComponent
                lblHello.setText("Hello " + name);
            }
            
        });
        framemain.setVisible(true); //window
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("Hello Me : Action");
    }
}
